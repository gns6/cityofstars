#!/usr/bin/python3

"""
    parse_wikitable.py

    MediaWiki Action API Code Samples
    Demo of `Parse` module: Parse a section of a page, fetch its table data and save
    it to a CSV file

    MIT license
"""

import csv
import requests
import re

S = requests.Session()
URL = "https://en.wikipedia.org/w/api.php"

def loop_through_pages():
    f = open("starNames.txt",'r')
    lines = f.readlines()
    f.close()
    constNum = 0
    for starName in lines:
        title = "List of stars in " + starName.strip("\n")
        params = {
            'action': "parse",
            'page': title,
            'prop': 'wikitext',
            'format': "json"
        }
        get_table(params, constNum)
        constNum = constNum + 1

def get_table(params, constNum):
    """ Parse a section of a page, fetch its table data and save it to a CSV file
    """
    print(params)
    res = S.get(url=URL, params=params)
    data = res.json()
    wikitext = data['parse']['wikitext']['*']
    lines = wikitext.split('|-')
    entries = []

    # Variable for finding which column # each value is in
    currIndex = 0
    raIndex = -1
    dIndex = -1
    apMagIndex = -1

    starCounter = 0

    for line in lines:
        line = line.strip()

        # Code for finding which column # each value is in
        if line.startswith("!"):
            table = line[2:].split('!')
            for entry in table:
                if 'Right' in entry.encode("utf-8"):
                    raIndex = currIndex
                if 'Declination' in entry.encode("utf-8"):
                    dIndex = currIndex
                if 'vis' in entry.encode("utf-8"):
                    apMagIndex = currIndex
                currIndex = currIndex + 1

        # Code for getting star information for stars brighter than 3
        elif line.startswith("|"):
            table = line[2:].split('||')
            entry = [constNum]

            #print(table[dIndex].replace('-', '').encode("utf-8").split("|")[1])

            print(table[apMagIndex].encode("utf-8"))

            if (not ("-" in table[apMagIndex].encode("utf-8")) and (float(re.sub('[^\d\.]', '', table[apMagIndex].encode("utf-8").split(".")[0]))> 3)):
                break
            entry.append(table[raIndex].encode("utf-8").split("|")[1])
            entry.append(table[raIndex].encode("utf-8").split("|")[2])
            entry.append(table[raIndex].replace('}', '').replace(' ', '').encode("utf-8").split("|")[3])

            ## Deal with negatives
            value = float(re.sub('[^\d\.]', '', table[dIndex].encode("utf-8").split("|")[1]))
            if "+" in table[dIndex].replace('-', '').encode("utf-8").split("|")[1]:
                value = value +90
            entry.append(value)
            entry.append(table[dIndex].encode("utf-8").split("|")[2])
            entry.append(table[dIndex].encode("utf-8").split("|")[3].replace('}', '').replace(' ', ''))
            entry.append(table[apMagIndex].encode("utf-8"))
            #print(entry)
            entries.append(entry)
            starCounter = starCounter + 1

    #Writing star data for this constellation to CSV if there are at least 3 stars in constellation
    if starCounter >= 3:
        with open("test.csv", "ab") as file:
            writer = csv.writer(file)
            writer.writerows(entries)

if __name__ == '__main__':
    loop_through_pages()
    #get_table()
