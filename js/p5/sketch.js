var img;
var imgg;
var mod;

function setup() {
  createCanvas(600, 600, WEBGL);
  img = loadImage("img/earth/4096_earth.jpg");
  imgg = loadImage("img/sun.jpg");
	//mod = loadModel("img/earth/earth.obj");
}

function draw() {
  background(200);
  //image(imgg, 0, 0, img.width/10, img.height/10);


  rotateZ(radians(23.5));
  rotateY(mouseX*.01);
  texture(img);
  sphere(200);

  //model(img);
}
