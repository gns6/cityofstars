// Parameters
var hostname = "mqtt.colab.duke.edu";
var port = 9001;  // used for secure websocekt (wss://) connections
var cwidth = 1024; // width of canvas
var cheight = 768; // height of canvas
var xpos;
var ypos;

function setup() {
  createCanvas(cwidth, cheight);
  noStroke();
  rectMode(CENTER);
  // create mqtt connection
  client = new Paho.MQTT.Client(hostname, port, "", "clientId");
  client.onConnectionLost = onConnectionLost;
  client.onMessageArrived = onMessageArrived; // msg parsing callback
  // connect the client using SSL and trigger onConnect callback
  client.connect({
    onSuccess: onConnect,
    useSSL: true
  });
  background(127);
}

function draw() {
  fill(0);
  ellipse(xpos, ypos, 10, 10);
}



function onConnect() {
  // Once a connection has been made, make subscription(s).
  console.log("onConnect");
  client.subscribe("/positionX");
  client.subscribe("/positionY");
};

function onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0)
	console.log("onConnectionLost:"+responseObject.errorMessage);
};

// mqtt incoming message parsing
function onMessageArrived(message) {
  console.log("onMessageArrived:"+message.destinationName+": "+message.payloadString);
  if (message.destinationName == "/positionX"){
    xpos = map(message.payloadString, 10, 1023, 0, cwidth);
  }
  if (message.destinationName == "/positionY"){
    ypos = map(message.payloadString, 10, 1023, 0, cheight);
  }
};
